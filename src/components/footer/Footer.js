import { Link } from "react-router-dom";
import { FaFacebookSquare, FaInstagram, FaLinkedinIn } from "react-icons/fa";

const Footer = () => {
  return (
    <footer className="h-20 px-6 py-2 bg-blue-100 text-blue-900">
      <div className="flex flex-col justify-between items-center container mx-auto md:flex-row mt-5">
        <Link to="/" className="text-2xl font-bold">
          <img
            src="https://tznext.herokuapp.com/blueTz.svg"
            className="w-13 h-8"
            alt="tz-logo"
          />
        </Link>
        <p className="mt-2 md:mt-0">All rights reserved 2021.</p>
        <div className="flex -mx-2 mt-4 mb-2 md:mt-0 md:mb-0">
          <a
            href="https://www.facebook.com/zobeyditaher/"
            className="mx-2 text-blue-900 hover:text-gray-400"
          >
            <FaFacebookSquare />
          </a>
          <a
            href="https://www.instagram.com/taherzi/"
            className="mx-2 text-red-500 hover:text-gray-400"
          >
            <FaInstagram />
          </a>
          <a
            href="https://www.linkedin.com/in/taherzobeydi/"
            className="mx-2 text-blue-900 hover:text-gray-400"
          >
            <FaLinkedinIn />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
