import { Link } from "react-router-dom";
import { FaRegStickyNote } from "react-icons/fa";
const Card = ({ blogs }) => {
  return (
    <>
      {blogs &&
        blogs.map((blog) => {
          return (
            <div className="mt-6 mx-2" key={blog.id}>
              <div className="max-w-2xl px-4 py-4 bg-white rounded-lg shadow-md">
                <div className="flex justify-between items-center">
                  <span className="font-light text-gray-600">Jun 1, 2020</span>
                  <Link
                    to="/"
                    className="px-2 py-1 bg-blue-900 text-gray-100 font-bold rounded hover:bg-blue-800"
                  >
                    <FaRegStickyNote />
                  </Link>
                </div>
                <div className="mt-2">
                  <Link
                    to="/"
                    className="text-2xl text-gray-700 font-bold hover:underline"
                  >
                    {blog.title}
                  </Link>
                  <p className="mt-2 text-gray-600">{blog.body}</p>
                </div>
                <div className="flex justify-between items-center mt-4">
                  <Link to="/" className="text-blue-500 hover:underline">
                    Read more
                  </Link>
                  <div>
                    <Link to="/" className="flex items-center">
                      <img
                        src={`https://randomuser.me/api/portraits/women/${blog.id}.jpg`}
                        alt="avatar"
                        className="mx-4 w-10 h-10 object-cover rounded-full hidden sm:block"
                      />
                      <Link
                        to="/"
                        className="text-gray-700 font-bold hover:underline"
                      ></Link>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
};

export default Card;
