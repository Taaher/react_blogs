import { Link } from "react-router-dom";

const Authors = ({ users }) => {
  return (
    <div className="px-8 fixed mt-16  xl:px-2 ">
      <h1 className="mb-4 text-xl font-bold text-gray-700">Authors</h1>
      <div className="flex flex-col bg-white max-w-sm px-6 py-4 mx-auto rounded-lg shadow-md">
        <ul className="-mx-4">
          {users &&
            users.map((user) => {
              return (
                <li className="flex items-center m-2 " key={user.id}>
                  <img
                    src={`https://randomuser.me/api/portraits/women/${user.id}.jpg`}
                    alt="avatar"
                    className="w-10 h-10 object-cover rounded-full mx-4"
                  />
                  <p>
                    <Link className="text-gray-700 font-bold mx-1 hover:underline">
                      {user.name}
                    </Link>
                    <span className="text-gray-700 text-sm font-light">
                      Created {user.id} Posts
                    </span>
                  </p>
                </li>
              );
            })}
        </ul>
      </div>
    </div>
  );
};

export default Authors;
