import { Link } from "react-router-dom";
const Category = () => {
  return (
    <div className="px-8 fixed mt-40">
      <h1 className="mb-4 text-xl font-bold text-gray-700 ">Categories</h1>
      <div className="flex flex-col bg-white px-4 py-6 max-w-sm mx-auto rounded-lg shadow-md fixed">
        <ul>
          <li>
            <Link
              to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - NodeJs
            </Link>
          </li>
          <li className="mt-2">
            <Link
              to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - ReactJs
            </Link>
          </li>
          <li className="mt-2">
            <Link
              to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - VueJs
            </Link>
          </li>
          <li className="mt-2">
            <Link
              to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - Tailwind
            </Link>
          </li>
          <li className="flex items-center mt-2">
            <Link
          to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - Python
            </Link>
          </li>
          <li className="flex items-center mt-2">
            <Link
              to="/"
              className="text-gray-700 font-bold mx-1 hover:text-gray-600 hover:underline"
            >
              - Django
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Category;
