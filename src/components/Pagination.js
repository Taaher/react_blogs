import {Link } from 'react-router-dom'

const Pagination = () => {
  return (
    <div className="mt-8">
      <div className="flex">
        <Link
          to="/"
          className="mx-1 px-3 py-2 bg-white text-gray-500 font-medium rounded-md cursor-not-allowed"
        >
          previous
        </Link>

        <Link
          to="/"
          className="mx-1 px-3 py-2 bg-white text-gray-700 font-medium hover:bg-blue-500 hover:text-white rounded-md"
        >
          1
        </Link>

        <Link
          to="/"
          className="mx-1 px-3 py-2 bg-white text-gray-700 font-medium hover:bg-blue-500 hover:text-white rounded-md"
        >
          2
        </Link>

        <Link
          to="/"
          className="mx-1 px-3 py-2 bg-white text-gray-700 font-medium hover:bg-blue-500 hover:text-white rounded-md"
        >
          3
        </Link>

        <Link
          to="/"
          className="mx-1 px-3 py-2 bg-white text-gray-700 font-medium hover:bg-blue-500 hover:text-white rounded-md"
        >
          Next
        </Link>
      </div>
    </div>
  );
};

export default Pagination;
