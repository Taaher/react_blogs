import { useEffect } from "react";
import Card from "./Card";
import Pagination from "./Pagination";
import Category from "./Category";
import Authors from "./Authors";
import { getPosts } from "../redux/blog/blogActions";
import { getAllUser } from "../redux/author/authorActions";

import { useSelector, useDispatch } from "react-redux";
const Main = () => {
  const users = useSelector((state) => state.author.authors);
  const blogs = useSelector((state) => state.blog.posts);
  const dispatch = useDispatch();

  console.log(users, blogs);
  useEffect(() => {
    dispatch(getPosts());
    dispatch(getAllUser());
  }, [dispatch]);

  return (
    <div className="bg-gray-100 min-h-screen">
      <div className="px-6 py-8">
        <div className="flex justify-between container mx-auto">
          <div className="mx-8 w-3/12  hidden lg:block">
            <Category />
          </div>
          <div className="w-full lg:w-8/12 mt-6">
            <Card blogs={blogs} />
            <Pagination />
          </div>
          <div className="mx-2 w-4/12  hidden lg:block">
            <Authors users={users} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
