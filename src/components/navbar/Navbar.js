import { Link } from "react-router-dom";
import { FaBars } from "react-icons/fa";
import { searchPost } from "../../redux/blog/blogActions";
import { useDispatch } from "react-redux";
import { useState, useEffect } from "react";

const Navbar = () => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");


  useEffect(() => {
    dispatch(searchPost(search));
  }, [dispatch, search]);

  
  return (
    <nav className="fixed w-full bg-white px-2 py-2 shadow">
      <div className="flex flex-col container mx-auto md:flex-row md:items-center md:justify-between xl:justify-between">
        <div className="flex justify-between items-center">
          <div>
            <Link
              to="/"
              className="text-gray-800 text-xl font-bold md:text-2xl"
            >
              <img
                src="https://tznext.herokuapp.com/blueTz.svg"
                className="w-13 h-8"
                alt="tz-logo"
              />
            </Link>
          </div>

          <div>
            <button
              type="button"
              className="block text-gray-800 hover:text-gray-600 focus:text-gray-600 focus:outline-none md:hidden"
            >
              <FaBars />
            </button>
          </div>
        </div>
        <div className="lg:w-5/12  xl:w-4/12 md:w-4/12  ">
          <input
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            className="border w-full h-12 rounded-lg focus:ring-2 focus:ring-blue-900 bg-gray-100 "
          />
        </div>
        <div className="md:flex flex-col md:flex-row md:-mx-4 hidden">
          <Link className="my-1 text-gray-800 hover:text-blue-500 md:mx-4 md:my-0">
            Home
          </Link>

          <button
            
            className="px-2 py-1 font-bold bg-blue-900 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:ring-opacity-50 rounded shadow-sm  text-white p-5"
          >
            Write a post
          </button>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
