import Footer from "./components/footer/Footer";
import Main from "./components/Main";
import Navbar from "./components/navbar/Navbar";
import { Router, Switch, Route } from "react-router-dom";
import History from "./History";

const Home = () => {
  return (
    <div>
      <Main />
      <Footer />
    </div>
  );
};

const App = () => {
  return (
    <Router history={History}>
      <Navbar />
      <Switch>
        <Route path="/" component={Home} />
      </Switch>
    </Router>
  );
};

export default App;
