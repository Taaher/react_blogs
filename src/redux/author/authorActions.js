import { GET_ALL_USER, GET_FAILED_USER } from "./authorTypes";
import axios from "axios";

export const getAllUser = () => async (dispatch) => {
  try {
    const response = await axios.get(
      "http://jsonplaceholder.typicode.com/users"
    );
    dispatch({ type: GET_ALL_USER, payload: response.data });
  } catch (error) {
    dispatch({ type: GET_FAILED_USER, payload: error });
  }
};
