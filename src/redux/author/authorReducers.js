import { GET_ALL_USER, GET_FAILED_USER } from "./authorTypes";

const initialState = {
  authors: [],
  errorMessage: "",
};

export const authorReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_USER:
      return {
        ...state,
        authors: action.payload,
      };
    case GET_FAILED_USER:
      return {
        ...state,
        authors: [],
        errorMessage: action.payload,
      };
    default:
      return state;
  }
};
