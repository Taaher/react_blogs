import { combineReducers, createStore, applyMiddleware } from "redux";
import reduxLogger from "redux-logger";
import reduxThunk from "redux-thunk";
import { blogReducers } from "./blog/blogReducers";
import { authorReducers } from "./author/authorReducers";

//configuration redux
export const configureStore = () => {
  const store = createStore(
    combineReducers({
      blog: blogReducers,
      author: authorReducers,
    }),
    applyMiddleware(reduxThunk, reduxLogger)
  );
  return store;
};
