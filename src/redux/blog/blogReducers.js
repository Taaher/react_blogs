import { GET_POSTS, SEARCH_POST } from "./blogTypes";

const initialState = {
  posts: [],
};

export const blogReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        posts: action.payload,
      };
    case SEARCH_POST:
      return {
        ...state,
        posts: action.payload,
      };
    default:
      return state;
  }
};
