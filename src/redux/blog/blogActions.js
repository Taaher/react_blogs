import { GET_POSTS, SEARCH_POST } from "./blogTypes";
import axios from "axios";

export const getPosts = () => async (dispatch) => {
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
    console.log("response!", res.data);
    dispatch({ type: GET_POSTS, payload: res.data });
  } catch (error) {
    console.log("error", error);
  }
};

export const searchPost = (content) => async (dispatch) => {
  try {
    const response = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    const res = response.data.filter((item) =>
      item.title.toLowerCase().includes(content.toLowerCase())
    );
    console.log("response search", res);
    dispatch({ type: SEARCH_POST, payload: res });
  } catch (error) {
    console.log("error", error);
  }
};
